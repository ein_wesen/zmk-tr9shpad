#include <behaviors.dtsi>
#include <dt-bindings/zmk/keys.h>
#include <dt-bindings/zmk/bt.h>

#define DEFAULT 0
#define NUM 1
#define MEDIA 2


	&sk {
		quick-release;
		lazy;
	};
	
/ {
	behaviors{
		tog_kp: tog_on_hold_kp_on_tap {
			compatible = "zmk,behavior-hold-tap";
			#binding-cells = <2>;
			flavor = "balanced";
			tapping-term-ms = <200>;
			quick-tap-ms = <200>;
			bindings = <&tog>, <&kp>;
		};

		kp_tog: kp_on_hold_tog_on_tap {
			compatible = "zmk,behavior-hold-tap";
			#binding-cells = <2>;
			flavor = "hold-preferred";
			tapping-term-ms = <200>;
			quick-tap-ms = <200>;
			bindings = <&kp>, <&tog>;
		};

		tog_tog: tog_on_hold_tog_on_tap {
			compatible = "zmk,behavior-hold-tap";
			#binding-cells = <2>;
			flavor = "tap-preferred";
			tapping-term-ms = <200>;
			quick-tap-ms = <200>;
			bindings = <&tog>, <&tog>;
		};

		kp_sk: kp_on_hold_stickey_on_tap {
			compatible = "zmk,behavior-hold-tap";
			#binding-cells = <2>;
			flavor = "hold-preferred";
			tapping-term-ms = <200>;
			quick-tap-ms = <200>;
			bindings = <&kp>, <&sk>;
		};

		td0: tap_dance_abc {
			compatible = "zmk,behavior-tap-dance";
			#binding-cells = <0>;
			tapping-term-ms = <400>;
			bindings = <&kp A>, <&kp B>, <&kp C>;
		};

		td1: tap_dance_def {
			compatible = "zmk,behavior-tap-dance";
			#binding-cells = <0>;
			tapping-term-ms = <400>;
			bindings = <&kp D>, <&kp E>, <&kp F>;
		};


		td2: tap_dance_ghi {
			compatible = "zmk,behavior-tap-dance";
			#binding-cells = <0>;
			tapping-term-ms = <400>;
			bindings = <&kp G>, <&kp H>, <&kp I>;
		};


		td3: tap_dance_jkl {
			compatible = "zmk,behavior-tap-dance";
			#binding-cells = <0>;
			tapping-term-ms = <400>;
			bindings = <&kp J>, <&kp K>, <&kp L>;
		};


		td4: tap_dance_mno {
			compatible = "zmk,behavior-tap-dance";
			#binding-cells = <0>;
			tapping-term-ms = <400>;
			bindings = <&kp M>, <&kp N>, <&kp O>;
		};


		td5: tap_dance_pqrs {
			compatible = "zmk,behavior-tap-dance";
			#binding-cells = <0>;
			tapping-term-ms = <400>;
			bindings = <&kp P>, <&kp Q>, <&kp R>, <&kp S>;
		};


		td6: tap_dance_tuv {
			compatible = "zmk,behavior-tap-dance";
			#binding-cells = <0>;
			tapping-term-ms = <400>;
			bindings = <&kp T>, <&kp U>, <&kp V>;
		};


		td7: tap_dance_wxyz {
			compatible = "zmk,behavior-tap-dance";
			#binding-cells = <0>;
			tapping-term-ms = <400>;
			bindings = <&kp W>, <&kp X>, <&kp Y>, <&kp Z>;
		};

		td_spec: tap_dance_special_chars {
			compatible = "zmk,behavior-tap-dance";
			#binding-cells = <0>;
			tapping-term-ms = <400>;
			bindings = <&kp PERIOD>, <&kp COMMA>, <&kp SEMI>, <&kp SQT>;
		};
	};

	keymap {
		compatible = "zmk,keymap";

		default_layer {
		// /*
		//	Left encoder: vertical scroll
		//	Right encoder: horizontal scroll
		//	-------------------------------
		//	|     |><:" |     |     |     |
		//	| ESC |.,;' | ABC | DEF | RET | 
		//	-------------------------------
		//	|     |     |     |     |MEDIA|
		//	| TAB | GHI | JKL | MNO | NUM |
		//	-------------------------------
		//	|     |     |     |     |     |
		//	|SHIFT|PQRS | TUV |WXYZ | ALT |
		//	-------------------------------
		//	|     |     |     |     |     |
		//	|CTRL | DEL |SPACE|BKSPC| CMD |
		//	-------------------------------

		// */
			bindings = <
				&kp ESC &td_spec &td0 &td1 &kp RET
				&kp TAB &td2 &td3 &td4 &tog_tog MEDIA NUM
				&kp_sk LSHIFT LSHIFT &td5 &td6 &td7 &kp_sk LALT LALT
				&kp_sk LCTRL LCTRL &kp DEL &kp SPACE &kp BSPC &kp_sk LCMD LCMD
			>;
			sensor-bindings = <&inc_dec_kp UP DOWN &inc_dec_kp LEFT RIGHT>; 		
		};

		number_layer {
		/*
			-------------------------------
			|     |  !  |  @  |  #  |     |
			| ESC |  1  |  2  |  3  | RET |
			-------------------------------
			|     |  $  |  %  |  ^  |MEDIA| 
			| TAB |  4  |  5  |  6  | NUM |
			-------------------------------
			|     |  &  |  *  |  (  |     |
			|SHIFT|  7  |  8  |  9  | ALT |
			-------------------------------
			|     |     |  )  |     |     |
			|CTRL | DEL |  0  |BSPC | CMD |
			-------------------------------
		*/
			bindings = <
				&trans &kp N1 &kp N2 &kp N3 &trans
				&trans &kp N4 &kp N5 &kp N6 &trans
				&trans &kp N7 &kp N8 &kp N9 &trans
				&trans &trans &kp N0 &trans &trans 
			>;
			sensor-bindings = <&inc_dec_kp HOME END &inc_dec_kp PG_UP PG_DN>; 		
		};

		media_layer {
		/*
			Left encoder: brightness
			Right encoder: volume
			-------------------------------
			| ESC |PREV |PLAY |NEXT | MUTE|
			-------------------------------
			| TAB |CSTOP|PAUSE|KSTOP|MEDIA|
			-------------------------------
			|SHIFT|BTPRV|SEL 1|BTNXT| ALT |
			-------------------------------
			|CTRL | DEL |BTCLR|BSPC | CMD |
			-------------------------------
		*/
			bindings = <&trans &kp K_PREV &kp K_PP &kp K_NEXT &kp K_MUTE
						&trans &kp C_STOP &kp C_PAUSE &kp K_STOP &tog MEDIA
						&trans &bt BT_PRV &bt BT_SEL 1 &bt BT_NXT &trans
						&trans &trans &bt BT_CLR &trans &trans
			>;
			sensor-bindings = <&inc_dec_kp C_BRI_DN C_BRI_UP &inc_dec_kp C_VOL_DN C_VOL_UP>;
		};
	};	
};

&pro_micro_i2c {
    status = "okay";

    oled: ssd1306@3c {
        compatible = "solomon,ssd1306fb";
        reg = <0x3c>;
        label = "DISPLAY";
        width = <128>;
        height = <32>;
        segment-offset = <0>;
        page-offset = <0>;
		display-offset = <0>;
        multiplex-ratio = <31>;
        //segment-remap;
        //com-invdir;
        com-sequential;
        prechargep = <0x22>;
		inversion-on;
    };
};
